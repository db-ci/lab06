import {connect} from 'mongoose';
import app from './app';

async function main() {
    try {
        const url = `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DATABASE}`;
        const cliente = await connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('Conectado com sucesso ao MongoDB');

        app.listen(app.get('port'), () => {
            console.log(`Express executando na porta: ${app.get('port')}`);
            console.log(`Express no modo: ${app.get('env')}`);
        });
    } catch (error) {
        console.log('Erro: ${error}');
    }
}

main();

//este diretório coloca tudo no ar

/*
interface Post {
    id?: number,
    email: string,
    first_name: string,
    last_name: string
}

const app = express();
const port = 3000;

app.use(consoleLogger);
app.use(bodyParser.json());

app.get('https://reqres.in/api/users?page=2', (req, res) => {
    const metodo = req.method;
    res.send(`Alô ${metodo}`);
});

app.get('https://reqres.in/api/users?page=2', (req, res) => {
    const nome = req.params.nome;
    res.send(`Alô ${nome}`);
});

app.post('https://reqres.in/api/users', (req, res) => {
    let post: Post = {
        email: "cintiavalente@email.com",
        first_name: "Cíntia",
        last_name: "Valente",
    };
    console.log(post);
});

const server = app.listen(port, () => {
    console.log(`Express na porta ${port}`);
});

function consoleLogger(req: express.Request, res: express.Response, next: express.NextFunction) {
    console.log(`${req.method} ${req.path}`);
    next();
}
*/

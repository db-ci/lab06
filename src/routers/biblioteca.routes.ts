import * as BibliotecaController from '../controllers/biblioteca.controller';
import { Router } from 'express';

export const router = Router();
export const path = '/livros'; //2. URI: /livros

//caminhos e métodos controlador:
router.get(`${path}/:id`, BibliotecaController.getLivro); //2. livros/{id}

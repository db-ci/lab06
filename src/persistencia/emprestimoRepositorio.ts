import { Emprestimo } from "../entidades/emprestimo";
import { EmprestimoModel } from "./emprestimoModel";

export class EmprestimoRepositorio { //inserir um novo empréstimo
    static async criar(emprestimo: Emprestimo): Promise<Emprestimo> {
        return EmprestimoModel.create(emprestimo); //retorna uma Promise
    }

    static async buscar(): Promise<Emprestimo[]> {
        return EmprestimoModel.find().exec();
    }
}
/*
    static async alterarDadosEmprestimo(): Promise<Emprestimo> {
        let res = EmprestimoModel.findOneAndUpdate({
            Livro: "O romance"
        }, {
            $set: {
                Autor: {
                'Cíntia Valente': String,
        }}
        {
            02/06/2020: Date,
            02/07/2020: Date
        },
        {
            new: true
    })
    */
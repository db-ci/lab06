import express from 'express'; 
import { router } from './routers/biblioteca.routes';
import errorHandler from 'errorhandler';
//import morgan from 'morgan';

const app = express();
//app.use(morgan('dev'));

//configuração middleware:
app.set('port', process.env.PORT);
app.use('/api', router);// /api/livros/:id
if (process.env.NODE_ENV === 'development') { //testa se o ambiente é de desenvolvimento
    app.use(errorHandler());
} 

export default app;

//este diretório configura o express, mostra as definições
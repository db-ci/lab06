import { Emprestimo } from "../entidades/emprestimo";
import { LivroRepositorio } from "../persistencia/livroRepositorio";
import { EmprestimoRepositorio } from "../persistencia/emprestimoRepositorio";
import { Livro } from "../entidades/livro";

export class Biblioteca {

    static async buscarLivroPorId(id: string): Promise<Livro|null> { //2 {id}
        return LivroRepositorio.buscarPorIdLivro(id);
    }

    static async buscarLivros(): Promise<Livro[]> { 
        return LivroRepositorio.buscar();
    }

    static async emprestarLivro(id: string): Promise<Emprestimo> {
        const livro = await LivroRepositorio.buscarPorIdLivro(id);
        if (livro !== null) {
            let emprestimo: Emprestimo = {
                livro: livro, 
                dataRetirada: new Date(Date.now()),
                dataEntrega : new Date(Date.now()+7*24*60*60*1000)
            };
            //cada dia tem 24h q tem 60 min, q tem 60 seg, q tem 1000 miliseg
            return await EmprestimoRepositorio.criar(emprestimo);
        } else {
            throw new Error('Livro não encontrado');
        }
    }
}

//este diretório é a camada de negócio, onde tem todas as funcionalidades
//é a camada de negócio que sabe o que fazer, passa tudo
//para o controler fazer request e requisições.



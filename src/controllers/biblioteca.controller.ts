import { Request, Response, NextFunction } from 'express';
import { Biblioteca } from '../negocio/biblioteca';

export async function getLivro(req: Request, res: Response, next: NextFunction) { //2. Verbo: GET
    try {
        const id = req.params.id; //2. Entrada: identificador do livro como String
        const livro = await Biblioteca.buscarLivroPorId(id);
        if (livro !== null) {
            res.json(livro); //2. 200 OK com objeto contendo os dados de um livro serializado em JSON
        } else {
            res.sendStatus(404); //2. 404 Not Found se o livro não existe
        }
    } catch (error) {
        next(error);
    }
}
